import { Component, inject, output, input, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Page } from '../../shared/page.model';
import { NgbPagination } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'rb-pagination',
  templateUrl: './pagination.component.html',
  styleUrl: './pagination.component.scss',
  imports: [NgbPagination],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaginationComponent {
  readonly page = input.required<Page<unknown>>();
  readonly pageChanged = output<number>();

  readonly navigate = input(false);

  private readonly router = inject(Router, { optional: true });

  onPageChanged($event: number) {
    const newPage = $event - 1;
    this.pageChanged.emit(newPage);

    if (this.navigate() && this.router) {
      this.router.navigate([], { queryParams: { page: newPage }, queryParamsHandling: 'merge' });
    }
  }
}
